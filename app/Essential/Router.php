<?php

namespace Fwk\Core\Essential;

use Fwk\Core\Exceptions\HttpException;
use Fwk\Core\Essential\View;

/**
 * @author Evgymart
 * @package Essential
 *
 * Router class that goes through controllers and adds them to the map
 */
final class Router
{
	private array $routeMap;

	private string $routesPath;

	public function __construct(private Application $app)
	{
	}

	public function route(string $routesPath): void
	{
		if (!is_readable($routesPath)) {
			die("Error");
		}

		$request = $this->app->request();
		$routeArray = require($routesPath);
		$map = $routeArray[$request->getMethod()][$request->getUri()] ?? null;
		if (is_null($map)) {
			throw new HttpException("Route Not Found",404);
		}

		$params = $request->getParams();
		if ($map['function'] ?? null) {
			$this->processFunction($map['function'], $params);
		}

		if ($map['controller'] ?? null) {
			$this->processController($map['controller'], $params);
		}
	}

	private function processFunction(Callable $func, array $params): void
	{
		if (!is_callable($func)) {
			die('503');
		}

		die($func($params));
	}

	private function processController(array $controller, array $params): void
	{
		$root = $this->app->root();
		$namespace = $this->app->appNamespace();

		$classname = $controller['class'] ?? '';
		$method = $controller['method'] ?? '';
		$path = "$root/Controllers/$classname.php";
		if (!is_readable($path)) {
			die('503 file not readable');
		}

		require_once $path;
		$class = "$namespace\Controllers\\$classname";
		if (!class_exists($class)) {
			die('503 Class does not exist');
		}

		$object = new $class();
		$return = $object->{$method}($params);
		$this->renderView($return);
	}

	private function renderView($view): void
	{
		if (is_string($view)) {
			die($view);
		}

		if ($view instanceof View) {
			$view->render();
		}
	}
}
