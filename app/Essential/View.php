<?php

namespace Fwk\Core\Essential;

/**
 * @author Evgymart
 * @package Essential
 *
 * View class
 */

final class View
{
	public function __construct(private string $view)
	{
    }

	public function render(): void
	{
		die($this->view);
	}
}
