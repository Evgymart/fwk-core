<?php

namespace Fwk\Core\Essential;

/**
 * @author Evgymart
 * @package Essential
 *
 * class for HTTP request management
 */
final class Request
{
	public function __construct()
	{
    }

    public function getMethod(): string
    {
		return strtoupper($_SERVER['REQUEST_METHOD']);
	}

	public function getUri(): string
	{
		return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	}

	public function getParams(): array
	{
		return match ($this->getMethod()) {
			'GET' => $_GET,
			'POST' => $_POST,
			default => []
		};
	}
}
