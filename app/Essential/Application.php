<?php

namespace Fwk\Core\Essential;

use Fwk\Core\Exceptions\HttpException;

/**
 * @author Evgymart
 * @package Essential
 *
 * Fwk supports multiple applications, don't make it a singleton.
 * Do not inherit.
 */
final class Application
{
	private Router $router;
	private Request $request;
	private string $routes;

	public function __construct(private string $root, private string $namespace)
	{
		$this->router = new Router($this);
		$this->request = new Request();
    }

	public function request(): Request
	{
		return $this->request;
	}

    public function root(): string
    {
        return $this->root;
	}

	public function appNamespace(): string
	{
		return $this->namespace;
	}

	public function registerRoutes(string $routesPath): void
	{
		$this->routes = $this->root . "/$routesPath";
	}

	public function run(): void
	{
		try {
			$this->router->route($this->routes);
		} catch (HttpException $e) {
			die("Http Exception");
		}
	}
}
