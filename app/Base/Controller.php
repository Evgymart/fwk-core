<?php

namespace Fwk\Core\Base;

use Fwk\Core\Essential\Application;

/**
 * @author Evgymart
 * @package Base
 *
 * Base controller class, all controllers need to extend it
 *
 */

abstract class Controller
{
	public static function resolve(Application $app): array
	{
	}

	public function __construct()
	{
	}
}
