<?php

namespace Fwk\Core\Exceptions;

class HttpException extends BaseException
{
	public function __construct(string $message, int $code)
	{
		parent::__construct($message, $code);
	}
}
